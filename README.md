Install nginx web server and deploy a phpinfo site.


Install list:
*  nginx
*  php
*  postgresql
*  git
*  ImageMagick
*  NodeJS
*  npm
*  Composer
*  Oracle InstantClient 19.3
    

PHP Modules:
*  BCMath 
*  Ctype
*  JSON 
*  Mbstring 
*  OpenSSL
*  PDO 
*  Tokenizer
*  XML 
*  oci8_12c
*  pgsql
*  pdo_pgsql
    
    

A new user is added with sudo access and sudo is enabled with ssh key.

Default nginx.conf is overwritten and a new site is created with parameters specified in server_prep.yml vars.

A php-fpm pool for the added user is created and the new site will run from the new pool.