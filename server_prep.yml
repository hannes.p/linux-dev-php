---
- hosts: all
  remote_user: entssh
  become: true
  
  vars:
    wwwuser: swis
    siteurl: dev.swis.modera.ee
    site2: front.dev.swis.modera.ee

  tasks:

    - name: Disable SELinux
      selinux:
        state: disabled

    - name: upgrade all packages
      yum:
        name: '*'
        state: latest
      
    - name: Add repositories
      shell: dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

    - name: Add remi epositories
      shell: dnf -y install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

    - name: Add powertools repo
      shell: dnf config-manager --set-enabled PowerTools

    - name: Install packages
      yum:
        name: 
          - nginx
          - wget
          - postgresql-contrib
          - postgresql-server
          - nodejs
          - npm
          - yum-utils
          - memcached
          - make
          - git
        state: latest

    - name: Install Imagemagick and dependencies
      yum:
        name: 
          - php-pear
          - php-devel
          - gcc 
          - ImageMagick
          - ImageMagick-devel
          - ImageMagick-perl
        state: latest

    - name: install php packages
      yum: 
        name:
          - php
          - php-fpm
          - php-devel
          - php-json
          - php-mbstring
          - php-mysqlnd
          - php-pdo
          - php-pgsql
          - php-xml
          - php-bcmath
          - php-ctype
          - php-openssl
          - php-tokenizer
          - php-ldap
        state: latest

    - name: Initialize the Database
      command: postgresql-setup initdb
      args:
        creates: /var/lib/pgsql/data

    - service:
        name: postgresql
        state: started
        enabled: yes

    - service:
        name: nginx
        state: started
        enabled: yes

    - service:
        name: php-fpm
        state: started
        enabled: yes

    - firewalld:
        service: http
        permanent: yes
        state: enabled

    - firewalld:
        service: https
        permanent: yes
        state: enabled

    - firewalld:
        port: 5432/tcp
        permanent: yes
        state: enabled

    - name: Add web user
      user:
        name: "{{ wwwuser }}"
        groups: wheel
        append: yes
        generate_ssh_key: yes
        ssh_key_bits: 2048
        ssh_key_file: .ssh/id_rsa

    - name: Create authorized_key file
      file:
        state: touch
        path: /home/{{ wwwuser }}/.ssh/authorized_keys
        owner: "{{ wwwuser }}"
        group: "{{ wwwuser }}"   
        mode: 0400
        modification_time: preserve
        access_time: preserve

    - name: Create php-fpm file
      copy:
        dest: /etc/php-fpm.d/{{ wwwuser }}.conf
        backup: yes
        content: |
          [{{ wwwuser }}]
          user = {{ wwwuser }}
          group = nginx
          listen = /var/run/fpm-{{ wwwuser }}.sock
          listen.owner = nginx
          listen.group = nginx
          listen.mode = 0660
          pm = dynamic
          pm.max_children = 2
          pm.start_servers = 1
          pm.min_spare_servers = 1
          pm.max_spare_servers = 2
          
    - name: Create nginx conf
      copy:
        dest: /etc/nginx/nginx.conf
        backup: yes
        content: |
          user  nginx;
          worker_processes  1;
          
          error_log  /var/log/nginx/error.log warn;
          pid        /var/run/nginx.pid;
          
          events {
              worker_connections  1024;
          }
          
          http {
              include       /etc/nginx/mime.types;
              default_type  application/octet-stream;
              
              log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                                '$status $body_bytes_sent "$http_referer" '
                                '"$http_user_agent" "$http_x_forwarded_for"';
                                
              access_log  /var/log/nginx/access.log  main;
              
              client_max_body_size 10M;
              
              sendfile        on;
              #tcp_nopush     on;
              
              keepalive_timeout  65;
              
              #gzip  on;
              
              include /etc/nginx/conf.d/*.conf;
              include /etc/nginx/sites/*.conf;
          }

    - name: Create nginx sites directory
      file: 
        path: /etc/nginx/sites
        state: directory
        mode: 0755

    - name: Create nginx site conf
      copy:
        dest: /etc/nginx/sites/{{ site2 }}.conf
        backup: yes
        content: |
          server {
          listen         80;
          server_name    {{ site2 }};
          root           /var/www/html/{{ site2 }};
          index          index.php;
          
          location / {
                          try_files $uri $uri/ /index.php?$query_string;
                                }
                                
          location ~* \.php$ {
              try_files $uri $uri/ /index.php$is_args$args;
          fastcgi_pass unix:/var/run/fpm-{{ wwwuser }}.sock;
          include         fastcgi_params;
          fastcgi_param   SCRIPT_FILENAME    $document_root$fastcgi_script_name;
          fastcgi_param   SCRIPT_NAME        $fastcgi_script_name;
            }
          }

    - name: Create nginx site conf
      copy:
        dest: /etc/nginx/sites/{{ siteurl }}.conf
        backup: yes
        content: |
          server {
          listen         80;
          server_name    {{ siteurl }};
          root           /var/www/html/{{ siteurl }}/public;
          index          index.php;
          
          location / {
                          try_files $uri $uri/ /index.php?$query_string;
                                }
                                
          location ~* \.php$ {
              try_files $uri $uri/ /index.php$is_args$args;
          fastcgi_pass unix:/var/run/fpm-{{ wwwuser }}.sock;
          include         fastcgi_params;
          fastcgi_param   SCRIPT_FILENAME    $document_root$fastcgi_script_name;
          fastcgi_param   SCRIPT_NAME        $fastcgi_script_name;
            }
          }


    - name: Create site home folder
      file: 
        path: /var/www/html/{{ siteurl }}
        state: directory
        mode: 0755
        owner: "{{ wwwuser }}"
        group: nginx

    - name: Create site 2 home folder
      file: 
        path: /var/www/html/{{ site2 }}
        state: directory
        mode: 0755
        owner: "{{ wwwuser }}"
        group: nginx

    - name: Create phpinfo site
      copy:
        dest: /var/www/html/{{ siteurl }}/public/phpinf.php
        owner: "{{ wwwuser }}"
        group: "{{ wwwuser }}"
        content: |
          <?php
          // Show all information, defaults to INFO_ALL
          phpinfo();
          ?>

    - name: Install Composer
      script: scripts/install_composer.sh
      
    - name: Move Composer globally
      command: mv composer.phar /usr/local/bin/composer

    - name: Set permissions on Composer
      file:
        path: /usr/local/bin/composer
        mode: "a+x"
        
    - name: Sudo without password
      replace:
        path: /etc/sudoers
        regexp: '# %wheel'
        replace: '%wheel'

    - name: Postgre from external
      lineinfile: 
        dest: /var/lib/pgsql/data/postgresql.conf
        regexp: "^#listen_addresses"
        line: "listen_addresses = '*'"
        state: present
        backup: yes

    - name: Grant users access from subnet
      postgresql_pg_hba:
        dest: /var/lib/pgsql/data/pg_hba.conf
        contype: host
        users: all
        source: 172.24.75.0/24
        databases: all
        method: md5

    - name: Grant users access from localhost
      postgresql_pg_hba:
        dest: /var/lib/pgsql/data/pg_hba.conf
        contype: host
        users: all
        source: 127.0.0.1/32
        databases: all
        method: md5


    - name: Install Oci8 dependencies
      script: scripts/install_oci8.sh

    - name: Install Oracle instantclient
      yum:
        name: oracle-instantclient19.3-devel
        state: latest

    - name: Install pecl oci8
      pear:
        name: pecl/oci8
        state: present

    - name: Enable oci8 module
      copy:
        dest: /etc/php.d/20-oci8.ini
        content: |
          extension=oci8.so

    - name: Restart service php-fpm
      service:
        name: php-fpm
        state: restarted

    - name: Restart service nginx
      service:
        name: nginx
        state: restarted    

    - name: Restart service firewalld
      service:
        name: firewalld
        state: restarted

    - name: Restart service postgresql
      service:
        name: postgresql
        state: restarted